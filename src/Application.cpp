#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) // it needs to be called after creating the context
        std::cout << "Glew is not okay" << std::endl;

    std::cout << glGetString(GL_VERSION) << std::endl;

    // Define Vertex buffer
    // glGenBuffers(1 = how many buffers you want to create, )
    unsigned int buffer; // this is the Id of the buffer
    glGenBuffers(1, &buffer);


    // now we populate the buffer with data
    // can even do it later..
    float positions[6] = {
        -0.5f, -0.5f,
         0.0f, 0.5f,
         0.5f, -0.5f
    };

    // http://docs.gl/gl4/glBufferData .. it's a static data , does not get modified..
    // size is 6 * the size of float as the vertex positions are written in float 
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), positions, GL_STATIC_DRAW);
    // so, the data is loaded, but we dont have a shader.. so the vertices will not be drawn

    // Attribe layout
    // we have to tell openGl how this data is laid out.. so far it only knows the positions
    // we can do it by GLAttributePointer
    // index = 0, size = 2 (because the number of object for this layout is 2 - couple of positions
    // type = float
    // normalized = false .. because they are already float and normalized
    // stride = total amount of bytes we have between each vertex.. 3 vertex.. each float is 4 bytes..
    // 1 vertex = 2 * 4 bytes...... so stride is "amount of byte we need to travel inorder to move to next vertex".. here 2 floats = 8 bytes
    // Pointer: we have one one attribute, the position.. so we pass 0
    // if we had more than one, we could pass the pointer offset of another attribute like 8
    // its a pointer
    //
    //Before setting the attrib, we must enable
    glEnableVertexAttribArray(buffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);

    // now we select the buffer
    // by passing buffer, we are selecting the buffer with this specific id..
    // if we had more buffer with separate id, we could do it too..
    // its an array type buffer.. there might be other type too
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);

        // now we draw the vertices from vertex buffer
        // we have 3 positions.. although there are 6 vertices. we laid it out like a couple
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // the other draw call is called glDrawElement, which is called from within the index buffer
        // will do it later when we use index buffer

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
